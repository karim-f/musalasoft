import {Inject, Input, ViewChild} from '@angular/core';
import { Injector } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Add_Gatway, Add_Vendor, Delete_Device, Get_All_Gateways } from 'src/app/config/APIs';
import { HttpService } from 'src/app/Services/http.service';
import { StatusEnum } from 'src/app/Shared/Enum/StatusEnum';
import { AddDeviceToGatewayVM } from 'src/app/Shared/Models/AddDeviceToGatewayVM';
import { Gateway } from 'src/app/Shared/Models/Gateway';
import { Peripheral } from 'src/app/Shared/Models/Peripheral';
import {getBaseUrl} from "../../main";


@Component({
  selector: 'app-gateway',
  templateUrl: './gateway.component.html',
  styleUrls: ['./gateway.component.css']
})
export class GatewayComponent implements OnInit {
  public httpService: HttpService;
  gateways : Gateway[] = [];
  peripherals : Peripheral[] = [];
  standBy : Peripheral[];
  isVisible = false;
  isVisible2 = false;

  @Input() newgateway: Gateway = new Gateway('','');
  @Input() newVendor: Peripheral = new Peripheral('','');

  gateway : Gateway = new Gateway('','');
  vendor : Peripheral = new Peripheral('','');
  vendorVm : AddDeviceToGatewayVM = new AddDeviceToGatewayVM(null);
  innerTable: any;

  constructor(public injector: Injector)
  {
  }

  ngOnInit(): void {
    this.httpService = this.injector.get(HttpService);
    this.httpService.GET(getBaseUrl() + Get_All_Gateways).subscribe(
      res => {
        this.gateways = res.data;
        this.gateways.forEach(element => {
          element.expand = false;
          element.peripheral.forEach(x => {
            x.statusName = StatusEnum[x.status];
          })
        });
      }
    );
  }

  showModal(id): void {
    this.vendorVm.GatewayId = id;
    this.isVisible = true;
  }

  handleOk(): void {
    console.log('Button ok clicked!');
    this.isVisible = false;
    this.isVisible2 = false;
  }

  handleCancel(): void {
    console.log('Button cancel clicked!');
    this.isVisible = false;
    this.isVisible2 = false;
  }

  onSubmit(form: NgForm)
  {
    debugger;
    this.vendor.Vendor = form.value.Vendor;
    this.vendorVm.Peripheral = this.vendor;
    this.httpService.POST(getBaseUrl() + Add_Vendor,this.vendorVm)
      .subscribe(res => {
        console.log(res);
      })
    this.handleOk();
    window.location.reload();
  }

  DeleteDevice(id)
  {
    this.httpService.Delete(getBaseUrl() + Delete_Device + `/` + id,id)
      .subscribe(res => {
        console.log(res);
        window.location.reload();
      })
  }

  AddGateway()
  {
    this.isVisible2 = true;
  }

  onSubmit2(form: NgForm){
    debugger;
    this.gateway.Name = form.value.Name;
    this.gateway.IPv4 = form.value.IPv4;
    this.httpService.POST(getBaseUrl() + Add_Gatway,this.gateway)
      .subscribe(res => {
        console.log(res);
      })
    this.handleOk();
    this.gateways.push(this.gateway);
    window.location.reload();
  }

}
