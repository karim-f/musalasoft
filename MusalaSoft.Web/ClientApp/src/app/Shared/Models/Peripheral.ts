import { Guid } from "guid-typescript";
import { StatusEnum } from "../Enum/StatusEnum";

export class Peripheral{
  key : number;
  Id : number;
  Vendor : string;
  CreatedAt : Date;
  status : StatusEnum;
  statusName : string;
  GatewayId : Guid;
  constructor(
    Vendor : string,
    statusName : string
  )
  {
    this.Vendor = Vendor;
    this.statusName = statusName;
  }
}
