import { Peripheral } from "./Peripheral";
import { Guid } from "guid-typescript";

export class AddDeviceToGatewayVM{
  GatewayId : Guid;
  Peripheral : Peripheral;
  constructor
  (
    GatewayId : Guid
  )
  {
    this.GatewayId = GatewayId;
  }
}
