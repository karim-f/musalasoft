import { Peripheral } from "./Peripheral";
import { Guid } from "guid-typescript";

export class Gateway{
  Id : Guid;
  Name : string;
  IPv4 : string;
  expand: boolean;
  peripheral : Peripheral[]=[];
  constructor
  (
    Name : string,
    IPv4 : string,
  )
  {
    this.Name = Name;
    this.IPv4 = IPv4;
  }
}
