import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GatewayComponent } from './gateway/gateway.component';
import { MatTableModule } from '@angular/material/table';
import { MatTreeModule } from '@angular/material/tree';
import { HttpModule } from '@angular/http';
import { ModuleMapLoaderModule } from '@nguniversal/module-map-ngfactory-loader';
import { HttpService } from './Services/http.service';
import { GridModule , GridAllModule } from '@syncfusion/ej2-angular-grids';
import { TreeGridModule , TreeGridAllModule } from '@syncfusion/ej2-angular-treegrid';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { IgxTreeGridModule,IgxExcelExporterService,IgxCsvExporterService } from "igniteui-angular";
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { en_US } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzBadgeModule} from "ng-zorro-antd/badge";
import {NavMenuComponent} from "./nav-menu/nav-menu.component";
import {FetchDataComponent} from "./fetch-data/fetch-data.component";

registerLocaleData(en);
@NgModule({
    declarations: [
        AppComponent,
        GatewayComponent,
        NavMenuComponent,
        FetchDataComponent
    ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatTreeModule,
    IgxTreeGridModule,
    GridModule,
    GridAllModule,
    TreeGridModule,
    TreeGridAllModule,
    FormsModule,
    HttpClientModule,
    NzTableModule,
    NzModalModule,
    NzButtonModule,
    NzBadgeModule,
    ModuleMapLoaderModule
  ],
  providers: [HttpService,IgxExcelExporterService,
    IgxCsvExporterService,
    { provide: NZ_I18N, useValue: en_US }],
  bootstrap: [AppComponent]
})
export class AppModule {
}
