import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { map, catchError } from 'rxjs/operators';
import { Http, Headers, RequestOptions } from '@angular/http';
import { throwError } from "rxjs";


@Injectable()
export class HttpService {
  private isAuthenticated:boolean;
  constructor(
    private router: Router,
    private http: Http,
  ) {
  }


  prepareRequestHeaders() {
    let headers: Headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Credentials', 'true');
    headers.append('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    return headers;
  }

  // POST request
  POST(url, data) {
    return this.http.post(url, data ? JSON.stringify(data) : {}, { headers: this.prepareRequestHeaders() })
      .pipe(
        map(res => res.json()),
        catchError(err => {
          if (err.status <= 400 || err.status === 500) {
            let errSTR = JSON.stringify(err);
            let errJSON = JSON.parse(errSTR);
            return throwError(errJSON._body);
          }
        })
      )

  }



  GET(url) {
    return this.http.get(url, { headers: this.prepareRequestHeaders() })
      .pipe(
        map(res => res.json()),
        catchError(err => {
          if (err.status <= 400 || err.status === 500) {
            let errSTR = JSON.stringify(err);
            let errJSON = JSON.parse(errSTR);
            return throwError(errJSON._body);
          }
        })
      )
  }

  Delete(url,data)
  {
    return this.http.delete(url, { params: data, headers: this.prepareRequestHeaders() })
      .pipe(
        map(res => res.json()),
        catchError(err => {
          if (err.status <= 400 || err.status === 500) {
            let errSTR = JSON.stringify(err);
            let errJSON = JSON.parse(errSTR);
            return throwError(errJSON._body);
          }
        })
      )
  }
}
