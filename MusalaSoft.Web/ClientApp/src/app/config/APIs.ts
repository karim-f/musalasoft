export const Get_All_Gateways   = `/api/v1/Gateway/GetAll`;
export const Add_Gatway         = `/api/v1/Gateway/CreateGateway`;
export const Add_Vendor         = `/api/v1/Gateway/CreateDevice`;
export const Delete_Device      = `/api/v1/Gateway/DeleteDevice`
