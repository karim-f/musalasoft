using Microsoft.AspNetCore.Mvc;

namespace MusalaSoft.Web.Controllers
{
    [ApiController]
    [Route("api/v{version:apiVersion}/[controller]")]
    public abstract class BaseApiController : ControllerBase
    {
    }
}