using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MusalaSoft.BAL.Services;
using MusalaSoft.DAL.Entities;
using MusalaSoft.Web.ViewModels;

namespace MusalaSoft.Web.Controllers.V1
{
    [ApiVersion("1.0")]
    public class GatewayController : BaseApiController
    {
        private readonly IGatewayService _gatewayService;

        public GatewayController(IGatewayService gatewayService) => (_gatewayService) = (gatewayService);
       
        [HttpPost("CreateGateway")]
        public async Task<ActionResult<GatewayResponse>> CreateGateway(Gateway gateway)
        {
            if (ModelState.IsValid)
            {
                var result = await _gatewayService.AddGatewayAsync(gateway);
                return result.Successed ?
                    new GatewayResponse { Code = 201, Data = result.Data, Message = "Created" } :
                    new GatewayResponse { Code = 503, Message = "503 Service Unavailable" };

            }
            else
                return new GatewayResponse { Code = 400, Data = ModelState.Values.SelectMany(v => v.Errors), Message = "Bad Request" };
        }
        [HttpPost("CreateDevice")]
        public async Task<ActionResult<GatewayResponse>> CreateDevice(AddDeviceToGatewayVM addDeviceToGatewayVm)
        {
            if (ModelState.IsValid)
            {
                var result = await _gatewayService.AddDeviceToGatewayAsync(addDeviceToGatewayVm.GatewayId , addDeviceToGatewayVm.Peripheral);
                return result.Successed ?
                    new GatewayResponse { Code = 201, Data = result.Data, Message = "Created" } :
                    new GatewayResponse { Code = 503, Message = "503 Service Unavailable" };

            }
            else
                return new GatewayResponse { Code = 400, Data = ModelState.Values.SelectMany(v => v.Errors), Message = "Bad Request" };
        }
        
        [HttpGet("GetAll")]
        public async Task<ActionResult<GatewayResponse>> GetGateways()
        {
            var result = await _gatewayService.GetAllGatewaysWithDevicesAsync();
            return result.Successed ?
                new GatewayResponse { Code = 200, Data = result.Data } :
                new GatewayResponse { Code = 404, Data = null, Message = "There are not Data yet!" };
        }
        
        [HttpDelete("DeleteDevice")]
        public async Task<ActionResult<GatewayResponse>> Delete(int id)
        { 
            var result = await _gatewayService.RemoveDeviceFromGatewayAsync(id);
            return result.Successed ?
                new GatewayResponse { Code = 200, Message = "Deleted" } :
                new GatewayResponse { Code = 503,  Data = ModelState.Values.SelectMany(v => v.Errors), Message = "Bad Request"};
        }
    }
}