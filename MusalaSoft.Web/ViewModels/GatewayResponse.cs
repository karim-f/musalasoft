namespace MusalaSoft.Web.ViewModels
{
    public class GatewayResponse
    {
        public int Code { get; set; }
        public object Data { get; set; }
        public string Message { get; set; }
    }
}