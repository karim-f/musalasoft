using System;
using MusalaSoft.DAL.Entities;

namespace MusalaSoft.Web.ViewModels
{
    public class AddDeviceToGatewayVM
    {
        public Guid GatewayId { get; set; }
        public Peripheral Peripheral { get; set; }
        
    }
}