using System;
using MusalaSoft.DAL.Enums;

namespace MusalaSoft.DAL.Entities
{
    public class Peripheral
    {
        public int Id { get; set; }
        public string Vendor { get; set; }
        public DateTime CreatedAt { get; set; }
        public StatusEnum Status { get; set; }
        public Guid GatewayId { get; set; }
        public Gateway Gateway { get; set; }
    }
}