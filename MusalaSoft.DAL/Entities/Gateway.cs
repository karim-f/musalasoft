using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MusalaSoft.DAL.Entities
{
    public class Gateway
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        
        public string Name { get; set; }
        
        //[Required(ErrorMessage = "IPv4 is required.")]
        [RegularExpression(@"^(?=\d+\.\d+\.\d+\.\d+$)(?:(?:25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]|[0-9])\.?){4}$",
            ErrorMessage = "Not Valid IPv4.")]
        public string IPv4 { get; set; }
        
        public ICollection<Peripheral> Peripheral { get; set; } 
        
    }
}