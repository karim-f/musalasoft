using System;
using Microsoft.Extensions.DependencyInjection;
using MusalaSoft.BAL.Services;
using MusalaSoft.DAL.Entities;
using MusalaSoft.DAL.Enums;
using Xunit;

namespace MusalaSoft.XUnitTest
{
    public class GatewayTest:IClassFixture<DbFixture>
    {
        private readonly ServiceProvider _serviceProvider;

        public GatewayTest(DbFixture fixture) => (_serviceProvider) = (fixture.ServiceProvider);
        
        [Fact]  
        public async void GetGateways()
        {
            using (var context = _serviceProvider.GetService<GatewayService>())
            {
                if (context != null)
                {
                    var result = await context.GetAllGatewaysWithDevicesAsync();
                    Assert.NotNull(result.Data);
                }
            }
        } 
        
        [Fact]  
        public async void AddGateway_WhenCalled_SaveData()  
        {
            using (var context = _serviceProvider.GetService<GatewayService>())
            {
                var gatewayObj = new Gateway()
                {
                    Name = "Gateway 2",
                    IPv4 = "192.168.1.0"
                };
                if (context != null)
                {
                    var result = await context.AddGatewayAsync(gatewayObj);
                    Assert.True(result.Successed);
                }
            }
        } 
        
        [Fact]  
        public async void AddGateway_WhenCalled_ReturnFalse()  
        {
            using (var context = _serviceProvider.GetService<GatewayService>())
            {
                var gatewayObj = new Gateway()
                {
                    Name = "Gateway 2",
                    IPv4 = "1.1.1.1.1"
                };
                if (context != null)
                {
                    var result = await context.AddGatewayAsync(gatewayObj);
                    Assert.False(result.Successed);
                }
            }
        }  
        
        [Fact]  
        public async void AddDeviceToGateway_WhenCalled_SaveData()
        {
            using (var context = _serviceProvider.GetService<GatewayService>())
            {
                var gatewayId = Guid.NewGuid();
                var peripheralObj = new Peripheral
                {
                    Vendor = "Vendor1",
                    Status = StatusEnum.online,
                    CreatedAt = DateTime.Now,
                    GatewayId = gatewayId
                };
                if (context != null)
                {
                    var result = await context.AddDeviceToGatewayAsync(gatewayId, peripheralObj);
                    Assert.False(result.Successed);
                }
            }
        } 
        [Fact]  
        public async void RemoveDeviceFromGateway_WhenCalled_SaveData()
        {
            using (var context = _serviceProvider.GetService<GatewayService>())
            {
                int id = 1;
                if (context != null)
                {
                    var result = await context.RemoveDeviceFromGatewayAsync(id);
                    Assert.True(result.Successed);
                }
            }
        } 
        
        [Fact]  
        public async void RemoveDeviceFromGateway_WhenCalled_ReturnFalse()  
        {
            using (var context = _serviceProvider.GetService<GatewayService>())
            {
                int id = 1000000;
                if (context != null)
                {
                    var result = await context.RemoveDeviceFromGatewayAsync(id);
                    Assert.False(result.Successed);
                }
            }
        } 
    }
}