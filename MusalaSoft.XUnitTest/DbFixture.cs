using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;

using MusalaSoft.DAL.Data;

namespace MusalaSoft.XUnitTest
{
    public class DbFixture
    {
        public IConfiguration Configuration { get; }
        public DbFixture()
        {
            var services = new ServiceCollection();
            services.AddDbContext<ApplicationDbContext>(options => 
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")),ServiceLifetime.Transient);
            ServiceProvider = services.BuildServiceProvider();
        }
        public ServiceProvider ServiceProvider { get; private set; }
    }
}