# MusalaSoft Task

**Please follow the steps to launch the project**

- run git clone https://gitlab.com/karim-f/musalasoft.git

- open appsettings.json in web project to change connection string and DependencyInjection class in DAL project

- go to clientApp folder in web to run npm install

- run the project no need to migrate database it will run automatically then seed data

- to launch swagger go to /swagger
