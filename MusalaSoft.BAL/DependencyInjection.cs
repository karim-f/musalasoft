using Microsoft.Extensions.DependencyInjection;
using MusalaSoft.BAL.BusinessRules;
using MusalaSoft.BAL.DataSeed;
using MusalaSoft.BAL.Services;

namespace MusalaSoft.BAL
{
    public static class DependencyInjection
    {
        public static void AddApplication(this IServiceCollection service)
        {
            service.AddTransient<IGatewayService, GatewayService>();
            service.AddTransient<IAppBusinessRules, AppBusinessRules>();
            service.AddTransient<IDatabaseInitializer, DatabaseInitializer>();

        }
    }
}