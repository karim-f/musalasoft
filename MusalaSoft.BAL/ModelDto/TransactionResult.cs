using System.Collections.Generic;

namespace MusalaSoft.BAL.ModelDto
{
    public class TransactionResult
    {
        public bool Successed { get; set; }
        public List<string> Errors { get; set; }
        public object Data { get; set; }
    }
}