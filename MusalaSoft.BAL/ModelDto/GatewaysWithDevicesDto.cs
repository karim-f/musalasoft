using System.Collections.Generic;
using MusalaSoft.DAL.Entities;

namespace MusalaSoft.BAL.ModelDto
{
    public class GatewaysWithDevicesDto
    {
        public List<Gateway> Gateways { get; set; }
        public List<Peripheral> Peripherals { get; set; }
    }
}