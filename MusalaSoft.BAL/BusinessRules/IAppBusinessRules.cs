using System;

namespace MusalaSoft.BAL.BusinessRules
{
    public interface IAppBusinessRules
    {
        bool IsGatewayReachedDevicesLimit(Guid id);
    }
}