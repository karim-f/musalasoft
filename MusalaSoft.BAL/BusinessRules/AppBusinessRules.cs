using System;
using System.Linq;
using MusalaSoft.DAL.Data;

namespace MusalaSoft.BAL.BusinessRules
{
    public class AppBusinessRules: IAppBusinessRules
    {
        private readonly ApplicationDbContext _db;

        public AppBusinessRules(ApplicationDbContext db) => (_db) = (db);
        
        public bool IsGatewayReachedDevicesLimit(Guid id)
        {
            var devicesCount = _db.Peripherals.Count(x => x.GatewayId == id);
            return devicesCount < 10;
        }
    }
}