using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MusalaSoft.BAL.ModelDto;
using MusalaSoft.DAL.Entities;

namespace MusalaSoft.BAL.Services
{
    public interface IGatewayService
    {
        Task<TransactionResult> GetAllGatewaysWithDevicesAsync();
        Task<TransactionResult> AddGatewayAsync(Gateway gateway);
        Task<TransactionResult> AddDeviceToGatewayAsync(Guid gatewayId, Peripheral peripheral);
        Task<TransactionResult> RemoveDeviceFromGatewayAsync(int id);
    }
}