using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MusalaSoft.BAL.BusinessRules;
using MusalaSoft.BAL.ModelDto;
using MusalaSoft.DAL.Data;
using MusalaSoft.DAL.Entities;

namespace MusalaSoft.BAL.Services
{
    public class GatewayService : IGatewayService , IDisposable
    {
        private readonly ApplicationDbContext _db;
        private readonly IAppBusinessRules _appBusinessRules;

        public GatewayService(ApplicationDbContext db, IAppBusinessRules appBusinessRules) 
            => (_db , _appBusinessRules) = (db , appBusinessRules);

        public async Task<TransactionResult> GetAllGatewaysWithDevicesAsync()
        {
            var data = await _db.Gateways.Include(x => x.Peripheral).ToListAsync();
            var result = new TransactionResult
            {
                Successed = true,
                Data = data
            };
            return result;
        }

        public async Task<TransactionResult> AddGatewayAsync(Gateway gateway)
        {
            var result = new TransactionResult();
            try
            {
                var gatewayObj = new Gateway
                {
                    Name = gateway.Name,
                    IPv4 = gateway.IPv4,
                };
                await _db.Gateways.AddAsync(gatewayObj);
                await _db.SaveChangesAsync();
                result.Successed = true;
                result.Data = gatewayObj;
                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<TransactionResult> AddDeviceToGatewayAsync(Guid gatewayId, Peripheral peripheral)
        {
            var result = new TransactionResult();
            try
            {
                if (_appBusinessRules.IsGatewayReachedDevicesLimit(gatewayId))
                {
                    var gateway = await _db.Gateways.FirstOrDefaultAsync(x => x.Id == gatewayId);
                    if (gateway != null)
                    {
                        var peripheralObj = new Peripheral
                        {
                            Vendor = peripheral.Vendor,
                            Status = peripheral.Status,
                            CreatedAt = DateTime.Now,
                            GatewayId = gateway.Id
                        };
                        await _db.AddAsync(peripheralObj);
                        await _db.SaveChangesAsync();

                        result.Successed = true;
                        result.Data = peripheralObj;
                    }
                    else
                        result.Errors.Add($"gateway not found");
                }
                else
                    result.Errors.Add("This gateway reached the limit devices");
                return result; 
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<TransactionResult> RemoveDeviceFromGatewayAsync(int id)
        {
            var result = new TransactionResult();
            try
            {
                var peripheral = await _db.Peripherals.FirstOrDefaultAsync(x => x.Id == id);
                if (peripheral != null)
                {
                    _db.Peripherals.Remove(peripheral);
                    result.Successed = true;
                }
                else
                {
                    result.Errors.Add($"Device #{id} not found");
                }
                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            
        }

        public void Dispose()
        {
            _db?.Dispose();
        }
    }
}