using System.Threading.Tasks;
using MusalaSoft.DAL.Data;

namespace MusalaSoft.BAL.DataSeed
{
    public class DatabaseInitializer :IDatabaseInitializer
    {
        private readonly ApplicationDbContext _db;
        public DatabaseInitializer(ApplicationDbContext db )  => ( _db) = ( db);
        public async Task SeedAsync()
        {
                var seedObj = new Seed(_db);
                await seedObj.SeedAll();
        }
    }
}