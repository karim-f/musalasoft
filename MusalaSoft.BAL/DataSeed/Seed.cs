using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MusalaSoft.DAL.Data;
using MusalaSoft.DAL.Entities;
using MusalaSoft.DAL.Enums;

namespace MusalaSoft.BAL.DataSeed
{
    public class Seed
    {
        private readonly ApplicationDbContext _db;

        public Seed(ApplicationDbContext db) => (_db) = ( db);

        public async Task SeedAll()
        {
            var gatewayObj = new Gateway
            {
                Name = "gateway 1",
                IPv4 = "192.168.1.0",
            };
            var peripheralsObj = new List<Peripheral>
            {
                new Peripheral()
                {
                    Vendor = "Vendor 1",
                    Status = StatusEnum.online,
                    CreatedAt = DateTime.Now,
                    GatewayId = gatewayObj.Id
                },
                new Peripheral()
                {
                    Vendor = "Vendor 2",
                    Status = StatusEnum.online,
                    CreatedAt = DateTime.Now,
                    GatewayId = gatewayObj.Id
                },
                new Peripheral()
                {
                    Vendor = "Vendor 3",
                    Status = StatusEnum.online,
                    CreatedAt = DateTime.Now,
                    GatewayId = gatewayObj.Id
                },
                new Peripheral()
                {
                    Vendor = "Vendor 4",
                    Status = StatusEnum.online,
                    CreatedAt = DateTime.Now,
                    GatewayId = gatewayObj.Id
                },
            };
            
            gatewayObj.Peripheral = peripheralsObj;
            await _db.Gateways.AddAsync(gatewayObj);
            await _db.SaveChangesAsync();
        }
    }
}