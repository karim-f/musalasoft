using System.Threading.Tasks;

namespace MusalaSoft.BAL.DataSeed
{
    public interface IDatabaseInitializer
    {
        Task SeedAsync();
    }
}